/**
 * Define a set of templats to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 *
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function() {
	// Define template paths to load
	const templatePaths = [
		"systems/woin/templates/parts/sheet-attributes.html",
		"systems/woin/templates/parts/sheet-groups.html"
	];

	// Load the template parts
	return loadTemplates(templatePaths);
};

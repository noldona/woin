/**
 * A simple and flexible system for world-building using an arbitrary collection of character and item attributes
 * Author: Noldona
 * Software License: MIT
 */

// Import Modules
import { WOINActor } from "./actor/actor.js";
import { WOINActorSheet } from "./actor/actor-sheet.js";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

/**
 * Init hook.
 */
Hooks.once("init", async function() {
	console.log(`W.O.I.N. | Initializing System`);

	// Place our classes in their own namespace for later reference
	// game.woin = {
	// 	WOINActor
	// };

	// Define custom Entity classes
	CONFIG.Actor.entityClass = WOINActor;

	// Register sheet application classes
	Actors.unregisterSheet("core", ActorSheet);
	Actors.registerSheet("woin", WOINActorSheet, {
		types: ["character", "old-character", "now-character", "new-character"],
		label: "WOIN.Sheet",
		makeDefault: true
	});
});

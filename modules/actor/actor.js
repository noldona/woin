import { EntitySheetHelper } from "../helper.js";

/**
 * Extend the base Actor entity by defining a custom roll data structure
 * which is ideal for the WOIN system.
 *
 * @extends {Actor}
 */
export class WOINActor extends Actor {
	/**
	 * Augment the basic actor data with additional dynamic data
	 *
	 * @override
	 */
	prepareData() {
		super.prepareData();
		console.log(`W.O.I.N. | Preparing Data`);

		const actorData = this.data;
		const data = actorData.data;
		const flags = actorData.flags;

		// Make separate methods for each Actor Type (character, npcs, etc.)
		// to keep things organized
		if (actorData.type === 'character') this._prepareCharacterData(actorData);
	}

	/**
	 * Prepare Character type specific data
	 */
	_prepareCharacterData(actorData) {
		const data = actorData.data;

		// Make modifications to data here. For example:

		// Loop through ability scores, and add their modifiers to our sheet output
		console.log(`W.O.I.N. | Caclulating Dice Pools`);
		for (let [key, attribute] of Object.entries(data.attributes)) {
			let val = attribute.value;
			let dice = 1;
			while (dice < data.advancement.grade) {
				if (val - dice >= 0) {
					val -= dice;
					dice++;
				} else {
					dice--;
					break;
				}
			}
			attribute.dice = dice;
		}
	}
}

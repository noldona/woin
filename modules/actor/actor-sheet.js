import { EntitySheetHelper } from "../helper.js";

/**
 * Extend the basic ActorSheet with some WOIN specific modifications
 *
 * @extends {ActorSheet}
 */
export class WOINActorSheet extends ActorSheet {

	/** @override */
	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			classes: ["woin", "sheet", "actor"],
			template: "systems/woin/templates/actor-sheet.html",
			width: 643,
			height: 863,
			tabs: [{
				navSelector: ".sheet-tabs",
				contentSelector: ".sheet-body",
				initial: "attributes"
			}],
			scrollY: [".biography", ".equipment", ".attacks"]
		});
	}

	/** @override */
	getData() {
		const data = super.getData();
		console.log(data);
		return data;
	}
};

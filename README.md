# WOIN for Foundry VTT
This project is an integration of the What's O.L.D. is N.E.W. (WOIN) RPG system in the Foundry VTT environment.

# Goals
The goal of this project is to support the O.L.D., N.O.W., and N.E.W. settings of the WOIN system.

# What is Foundry VTT?
Foundry VTT is a virtual tabletop application for playing RPG games over the internet. More information can be found at https://foundryvtt.com/

# What is WOIN?
The What's O.L.D. is N.E.W. rules and information can be found at http://www.woinrpg.com/.
All content used in the system is under the open games license version 1.0a, found at http://www.woinrpg.com/open-gaming-license.
